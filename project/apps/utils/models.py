# coding=utf-8
from __future__ import unicode_literals
import itertools

from django.db import models

from pytils import translit


# ***************************************************  SLUG MIXIN  *************************************************** #
class SlugMixin(models.Model):
    SLUG_LENGTH = 45
    SLUG_URL_KWARG = 'slug'
    DETAIL_URL_NAME = None
    SLUG_SOURCE_FIELDS = ()
    SLUG_FIELD_VERBOSE_NAME = 'ЧПУ'

    slug = models.SlugField(SLUG_FIELD_VERBOSE_NAME, unique=True)

    class Meta:
        abstract = True

    @property
    def slug_length(self):
        if not self.SLUG_LENGTH:
            self._raise_field_error('SLUG_LENGTH')
        return self.SLUG_LENGTH

    def _get_field_value(self, field_name, *args, **kwargs):
        attr = getattr(self, field_name)
        return attr if not hasattr(attr, '__call__') else attr(*args, **kwargs)

    def get_slug_source_value(self, *args, **kwargs):
        if not self.SLUG_SOURCE_FIELDS:
            self._raise_field_error('SLUG_SOURCE_FIELDS')
        return ' '.join(
            map(
                lambda field: self._get_field_value(field, *args, **kwargs),
                self.SLUG_SOURCE_FIELDS
            )
        )

    def create_slug(self, name):
        slug = title = translit.slugify(name)[:self.slug_length]
        objects = self.__class__.objects
        if objects.filter(slug=title).exists() and (not self.pk or objects.get(slug=slug).pk != self.pk):
            # fixme: Possible endless iteration
            for i in itertools.count(1):
                if not objects.filter(slug=slug).exists():
                    break
                slug = '{0}-{1}'.format(title, i)
        return slug

    def _raise_field_error(self, field_name):
        raise ValueError('Value of {0} is not specified for the model {1}'.format(field_name, self.__class__.__name__))

    @models.permalink
    def get_absolute_url(self):
        if not self.DETAIL_URL_NAME:
            self._raise_field_error('DETAIL_URL_NAME')
        elif not self.SLUG_URL_KWARG:
            self._raise_field_error('SLUG_URL_KWARG')
        return (self.DETAIL_URL_NAME, (), {self.SLUG_URL_KWARG: self.slug})

    def populate_slug(self):
        if not self.slug:
            self.slug = self.create_slug(self.get_slug_source_value())

    # def save(self, *args, **kwargs):
    #     self.populate_slug()
    #     super(SlugMixin, self).save(*args, **kwargs)


# **************************************************  SMALL MIXINS  ************************************************** #
class PositionMixin(models.Model):
    order = models.PositiveSmallIntegerField(verbose_name='Порядок', default=0)

    class Meta:
        abstract = True
        ordering = ('order', )


class EnabledMixin(models.Model):
    is_enabled = models.BooleanField(verbose_name='Активно', default=True)

    class Meta:
        abstract = True


class CreateUpdateDatesMixin(models.Model):
    created_at = models.DateTimeField('Дата создания', auto_now_add=True)
    updated_at = models.DateTimeField('Дата последнего изменения', auto_now=True)

    class Meta:
        abstract = True
