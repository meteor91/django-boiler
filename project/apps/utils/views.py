# coding=utf-8

from django.http import HttpResponse, Http404
from django.views.generic import View, TemplateView
from django.views.generic.edit import FormMixin



class SingletonDetailMixin:
    def get_object(self, queryset=None):
        return self.model.get_solo()

# ***********************************************  JSON RESPONSE MIXIN  ********************************************** #
from raven.utils import json


class JSONResponseMixin(object):
    """
    A mixin that can be used to render a JSON response.
    """
    STATUS_OK = u'OK'
    STATUS_FAIL = u'FAIL'

    class response_status(object):
        success = u'success'
        fail = u'fail'

    def render_to_json_response(self, status, data=None, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        if not data:
            data = {}
        response = {
            u'status': status,
            u'data': data
        }
        return HttpResponse(
            self.convert_context_to_json(response),
            content_type=u'application/json',
            **response_kwargs
        )

    def convert_context_to_json(self, context):
        """Convert the context dictionary into a JSON object"""
        # Note: This is *EXTREMELY* naive; in reality, you'll need
        # to do much more complex handling to ensure that arbitrary
        # objects -- such as Django model instances or querysets
        # -- can be serialized as JSON.
        return json.dumps(context)


# *************************************************  AJAX FORM VIEW  ************************************************* #
class AjaxFormView(FormMixin, View, JSONResponseMixin):
    def get(self, request, *args, **kwargs):
        raise Http404

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        form = self.get_form()

        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    # PUT is a valid HTTP verb for creating (with a known URL) or editing an
    # object, note that browsers only support POST for now.
    def put(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def form_valid(self, form):
        form.save()
        return self.render_to_json_response(
            status=self.response_status.success,
            data=u'Success'
        )

    def form_invalid(self, form):
        errors = {name: form[name].errors for name in form.fields.iterkeys()}
        return self.render_to_json_response(
            status=self.response_status.fail,
            data={
                'message': 'Error on saving form',
                'errors': errors,
                'general_errors': form.non_field_errors(),
            }
        )