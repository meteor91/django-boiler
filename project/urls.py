# coding=utf-8
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView

admin.autodiscover()

urlpatterns = [
    url(r'^robots\.txt$', TemplateView.as_view(template_name='robots.txt', content_type='text/plain')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin_tools/', include('admin_tools.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# 3rd-party apps
urlpatterns += []

# Project apps
urlpatterns += [

]

if settings.DEBUG:
    urlpatterns += [
    
    ]

# Staticfiles
urlpatterns += staticfiles_urlpatterns()