# coding=utf-8
from __future__ import unicode_literals


class DevelopmentEmailSettings(object):
    EMAIL_USE_TLS = True
    EMAIL_SENDER = ''
    EMAIL_HOST = ''
    EMAIL_HOST_PASSWORD = ''
    EMAIL_PORT = 587
    EMAIL_HOST_USER = ''


class StagingEmailSettings(object):
    EMAIL_USE_TLS = True
    EMAIL_SENDER = ''
    EMAIL_HOST = ''
    EMAIL_HOST_PASSWORD = ''
    EMAIL_PORT = 587
    EMAIL_HOST_USER = ''


class ProductionEmailSettings(object):
    EMAIL_SENDER = ''
    EMAIL_HOST_USER = ''
    EMAIL_HOST = ''
    EMAIL_HOST_PASSWORD = ''
    EMAIL_PORT = 587
    EMAIL_USE_TLS = True
